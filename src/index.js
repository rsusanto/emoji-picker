import delegate from 'delegate';
import memoize from 'lodash.memoize';
import debounce from 'lodash.debounce';
import categories from '../data/categories.json';
import emoji from '../data/emoji.json';
import iconSmiley from '../icons/smiley.svg';
import iconNature from '../icons/nature.svg';
import iconFood from '../icons/food.svg';
import iconActivity from '../icons/activity.svg';
import iconTravel from '../icons/travel.svg';
import iconObject from '../icons/object.svg';
import iconSymbol from '../icons/symbol.svg';
import iconFlag from '../icons/flag.svg';

const TEMPLATE = `
<div class="emoji-picker">
	<div class="emoji-picker-header">
		<span data-id="0">${ iconSmiley }</span>
		<span data-id="1">${ iconNature }</span>
		<span data-id="2">${ iconFood }</span>
		<span data-id="3">${ iconActivity }</span>
		<span data-id="4">${ iconTravel }</span>
		<span data-id="5">${ iconObject }</span>
		<span data-id="6">${ iconSymbol }</span>
		<span data-id="7">${ iconFlag }</span>
	</div>
	<div class="emoji-picker-input"><input type="text" placeholder="Search" /></div>
	<div class="emoji-picker-table"></div>
	<div class="emoji-picker-footer"></div>
</div>`;

/** @class */
class EmojiPicker {
	/**
	 * Class constructor.
	 *
	 * @param {Object} opts
	 * @param {HTMLElement} opts.button
	 * @param {HTMLElement} opts.container
	 * @param {Function} opts.callback
	 */
	constructor( opts = {} ) {
		/** @type {Object} */ this.opts = opts;
		/** @type {HTMLElement} */ this.button = opts.button;
		/** @type {HTMLElement} */ this.container = opts.container;
		/** @type {Function} */ this.callback = opts.callback;

		this.attachEvents();
	}

	/**
	 * Attach required event handlers.
	 */
	attachEvents() {
		this.button.addEventListener( 'click', () => this.toggle() );

		delegate( this.container, '.emoji-picker-item', 'click', e => this.onSelect( e ) );
		delegate( this.container, '[type=text]', 'input', e => this.onFilter( e ) );
	}

	/**
	 * Get popup element.
	 *
	 * @function
	 * @returns {HTMLElement}
	 */
	getPicker = memoize( () => {
		const wrapper = document.createElement( 'div' );
		wrapper.innerHTML = TEMPLATE;

		const picker = wrapper.children[ 0 ];
		picker.style.display = 'none';
		this.container.appendChild( picker );

		return picker;
	} );

	/**
	 * Get popup header element.
	 *
	 * @function
	 * @returns {HTMLElement}
	 */
	getHeader = memoize( () => {
		return this.getPicker().querySelector( '.emoji-picker-header' );
	} );

	/**
	 * Get search input element.
	 *
	 * @function
	 * @returns {HTMLElement}
	 */
	getInput = memoize( () => {
		return this.getPicker().querySelector( '[type=text]' );
	} );

	/**
	 * Get emoji table element.
	 *
	 * @function
	 * @returns {HTMLElement}
	 */
	getTable = memoize( () => {
		return this.getPicker().querySelector( '.emoji-picker-table' );
	} );

	/**
	 * Get emoji item.
	 *
	 * @function
	 * @param {Array} item
	 * @returns {string}
	 */
	getItem = item => {
		let name = item[ 0 ],
			codes = item[ 1 ].split( '-' ).map( hex => parseInt( hex, 16 ) ),
			unicode = String.fromCodePoint.apply( null, codes ),
			shortNames = ':' + item[ 2 ].replace( /\s/g, ': :' ) + ':';

		return (
			`<span class="emoji-picker-item" title="${ name }"` +
			` data-title="${ name }" data-names="${ shortNames }"` +
			` data-unicode="${ unicode }">${ unicode }</span>`
		);
	};

	/**
	 * Get popup footer element.
	 *
	 * @function
	 * @returns {HTMLElement}
	 */
	getFooter = memoize( () => {
		return this.getPicker().querySelector( '.emoji-picker-footer' );
	} );

	/**
	 * Toggle popup element visibility.
	 */
	toggle() {
		if ( this.getPicker().style.display === 'none' ) {
			this.show();
		} else {
			this.hide();
		}
	}

	/**
	 * Show popup.
	 */
	show() {
		this.getPicker().style.display = '';
		this.autohide( true );

		let query = this.getInput().value.trim();
		this.render( this.filter( query ) );
	}

	/**
	 * Hide popup.
	 */
	hide() {
		this.getPicker().style.display = 'none';
		this.autohide( false );
	}

	/**
	 * Setup autohide, which automatically hide popup when click event occurs outside of it.
	 *
	 * @param {boolean} install
	 */
	autohide( install = false ) {}

	/**
	 * Filter emoji.
	 *
	 * @param {string} query
	 * @returns {Array.<Array.<*>>}
	 */
	filter( query = '' ) {
		query = query.toLowerCase().trim();
		if ( ! query ) {
			return emoji;
		}

		return emoji.map( items => {
			return items.filter( item => {
				// Query match with emoji name.
				if ( item[ 0 ].toLowerCase().indexOf( query ) > -1 ) {
					return true;
				}

				// Query match with one of emoji short names.
				let match = false;
				item[ 2 ].split( ' ' ).forEach( shortName => {
					if ( shortName.toLowerCase().indexOf( query ) > -1 ) {
						match = true;
					}
				} );
				return match;
			} );
		} );
	}

	/**
	 * Render emoji.
	 *
	 * @param {Array.<Array.<*>>} data
	 */
	render( data = [] ) {
		let html = data.map( ( items, index ) => {
			if ( ! items.length ) {
				return '';
			}

			return (
				'<div>' +
				`<div class="emoji-picker-category" data-category="${ index }">${
					categories[ index ]
				}</div>` +
				items.map( item => this.getItem( item ) ).join( '' ) +
				'</div>'
			);
		} );

		// Defer render emoji.
		setTimeout( () => {
			this.getTable().innerHTML = html.join( '' );
		}, 0 );
	}

	/**
	 * Select emoji.
	 */
	selectEmoji() {}

	/**
	 * Select emoji category.
	 */
	selectCategory() {}

	/**
	 * Set emoji preview.
	 */
	setPreview() {}

	/**
	 * Unset emoji preview.
	 */
	unsetPreview() {}

	/**
	 * Scroll spy.
	 *
	 * @function
	 */
	scrollSpy = debounce( () => {} );

	/**
	 * Handle select emoji action
	 *
	 * @private
	 * @param {Event} e
	 */
	onSelect( e ) {
		this.hide();

		if ( typeof this.callback === 'function' ) {
			this.callback( {
				title: e.target.getAttribute( 'data-title' ),
				names: e.target.getAttribute( 'data-names' ),
				unicode: e.target.getAttribute( 'data-unicode' )
			} );
		}
	}

	/**
	 * Handle search emoji action.
	 *
	 * @private
	 * @param {Event} e
	 */
	onFilter = debounce( e => {
		let query = e.target.value.trim();
		this.render( this.filter( query ) );
	}, 500 );
}

module.exports = EmojiPicker;
