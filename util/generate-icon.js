const fs = require( 'fs' );
const { icon } = require( '@fortawesome/fontawesome-svg-core' );

// FontAwesome icons to be generated.
const icons = [
	[ 'smile', 'regular', 'smiley' ],
	[ 'leaf', 'solid', 'nature' ],
	[ 'utensils', 'solid', 'food' ],
	[ 'football-ball', 'solid', 'activity' ],
	[ 'plane', 'solid', 'travel' ],
	[ 'lightbulb', 'solid', 'object' ],
	[ 'dollar-sign', 'solid', 'symbol' ],
	[ 'flag', 'regular', 'flag' ]
];

const toTitleCase = str => {
	return str
		.split( '-' )
		.map( str => str.toLowerCase().replace( /^\w/, c => c.toUpperCase() ) )
		.join( '' );
};

const getHtml = ( name, type ) => {
	const fa = require( `@fortawesome/free-${ type }-svg-icons` );
	const faIcon = fa[ 'fa' + toTitleCase( name ) ];

	let html = '';

	if ( faIcon ) {
		html = icon( faIcon ).html[ 0 ].trim();
		// Remove unused tag attributes.
		html = html
			.replace( /\s+data-prefix="[^"]+"/, '' )
			.replace( /\s+data-icon="[^"]+"/, '' )
			.replace( /\s+class="[^"]+"/, '' );
	}

	return html;
};

// Save svg of icons.
icons.forEach( icon => {
	let html = getHtml( ...icon );
	fs.writeFileSync( `./icons/${ icon[ 2 ] || icon[ 0 ] }.svg`, html, 'utf8' );
} );

console.log( 'Icons were saved!' );
