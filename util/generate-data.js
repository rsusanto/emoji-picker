const fs = require( 'fs' );
const request = require( 'request' );
const categories = require( '../data/categories.json' );
const url = 'https://raw.githubusercontent.com/iamcal/emoji-data/master/emoji.json';

// https://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
const toTitleCase = function( str ) {
	return str.replace( /\w\S*/g, function( txt ) {
		return txt.charAt( 0 ).toUpperCase() + txt.substr( 1 ).toLowerCase();
	} );
};

request.get( { url, json: true }, ( err, res, data ) => {
	if ( err || res.statusCode !== 200 ) {
		return;
	}

	let emoji = [],
		emojiByCategory = [];

	// Filter emoji.
	data.forEach( item => {
		let { name, unified, short_names, category, sort_order, has_img_apple, has_img_google } = item,
			categoryIndex = categories.indexOf( category );

		// Exclude emoji not in category list, or not natively available
		// on iOS and Android.
		if ( categoryIndex === -1 || ! has_img_apple || ! has_img_google ) {
			return;
		}

		emoji.push( [ name, unified, short_names, categoryIndex, sort_order ] );
	} );

	// Sort emoji.
	emoji.sort( ( a, b ) => {
		let aWeight = a[ 3 ] * 1000 + a[ 4 ],
			bWeight = b[ 3 ] * 1000 + b[ 4 ];

		return aWeight - bWeight;
	} );

	// Group emoji by category.
	emoji.forEach( item => {
		let index = item[ 3 ],
			name = toTitleCase( item[ 0 ] || item[ 2 ][ 0 ].replace( /[-_]/g, ' ' ) );

		emojiByCategory[ index ] = emojiByCategory[ index ] || [];
		emojiByCategory[ index ].push( [ name, item[ 1 ], item[ 2 ].join( ' ' ) ] );
	} );

	// Save data to json file.
	emojiByCategory = JSON.stringify( emojiByCategory );
	fs.writeFileSync( './data/emoji.json', emojiByCategory, 'utf8' );

	console.log( 'Data was saved!' );
} );
